
module.exports = {
      client: 'mysql',
      connection: { 
        socketPath: process.env.DATABASE_SOCKET, 
        host: process.env.DATABASE_HOST, 
        user: process.env.DATABASE_USER, 
        password: process.env.DATABASE_PASSWORD, 
        database: process.env.DATABASE_NAME 
      },
        migrations: {
        tableName: 'knex_migrations'
      }
  }
