import express from "express";
import dotenv from "dotenv"
import db from "../../database/connection.js"

dotenv.config()

const router = express.Router();


/**Get Projects**/
router.get("/", function (req, res) {
    db.query(
      "SELECT * FROM projects",
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });


/**Update/Edit Project**/
router.put("/", (req, res) => {
  const { title, description } = req.body;
  db.query(
    `UPDATE projects SET title="${title}", description="${description}" WHERE projectID=${req.body.projectID}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

export default router;