import express from "express";
import { v4 as uuidv4 } from "uuid"; //ID encryption
import dotenv from "dotenv"
import db from "../../database/connection.js"

dotenv.config()

const router = express.Router();

const validateEntries = (req, res, next) => {
  const body = req.body
  const invalid = []
  for (var property in body) {
      if (body[property] == null || body[property].length === 0) {
          invalid.push(property)
      }
  }
  const requiredProperties = ["name", "email", "phoneNumber", "content"]
  requiredProperties.filter(prop => !body.hasOwnProperty(prop)).forEach(key => invalid.push(key))
  if (invalid.length > 0) {
      return res.status(400).send({message: "validation error", invalid})
  }
  next()
}

const addEntry = (data) => {
    const { entryID, name, email, phoneNumber, content } = data
      return new Promise ((resolve, reject) => {
        db.query(
        `INSERT INTO contactentries (entryID, name, email, phoneNumber, content) VALUE ("${entryID}", "${name}", "${email}", "${phoneNumber}", "${content}")`,
        (error, results) => {
          if (error){
            console.log(error)
            return reject(error)
          } else {
            return resolve(results)
          }
        }
      )
    })
  }


//Add new entry
router.post('/entries', validateEntries, async (req, res) => {
  const body = req.body
  const newEntry = {entryID: uuidv4(), ...body}
  addEntry(newEntry)
  return res.status(201).json(newEntry)
})

//Get list of entries
router.get("/entries", function (req, res) {
  db.query(
    "SELECT * FROM contactentries",
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

export default router;