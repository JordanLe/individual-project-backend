import express from "express";
import { v4 as uuidv4 } from "uuid"; //ID encryption
import bcrypt from "bcrypt" //bcrypt for hashing passwords
import dotenv from "dotenv"
import db from "../../database/connection.js"

dotenv.config()

const router = express.Router();

const validateUserContents = (request) => {
    const errors = []
    const requiredProperties = ["name", "password", "email"]
    // check if each property is expected
    requiredProperties.forEach(property => {
        // does property exist on req.body
        if(!request.hasOwnProperty(property)) {
            errors.push(property)
        }
    })
    // valid password
    if (request.password.length < 8) {
        errors.push("password")
    }
    // valid email
    if (request.email.length) {
        var atpos=request.email.indexOf("@");
        var dotpos=request.email.lastIndexOf(".");
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=request.email.length) {
            errors.push("email")
        }
    }
    return errors
}

const addUser = (data) => {
    const { userID, name, password, email } = data
      return new Promise ((resolve, reject) => {
        db.query(
        `INSERT INTO user (userID, name, email, password) VALUE ("${userID}", "${name}", "${email}", "${password}")`,
        (error, results) => {
          if (error){
            console.log(error)
            return reject(error)
          } else {
            return resolve(results)
          }
        }
      )
    })
  }

//Add new user
router.post("/", (req, res) => {
    let errors = []
    errors = [ ...errors, ...validateUserContents(req.body)]

    if (errors.length) {
        return res.status(400).send({message: "validation error", invalid: errors })
    }
    else {
        let newUser = {
            userID: uuidv4(),
            name: req.body.name,
            password: req.body.password,
            email: req.body.email,
        }
        bcrypt.hash(newUser.password, 10, function (err, hash) {
            newUser.password = hash;
            //Push new user into database
            addUser(newUser)
            return res.status(201).json(newUser)
        })

    }
})


export default router;