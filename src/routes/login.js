import express from "express"
import jwt from "jsonwebtoken"
import bcrypt from 'bcrypt'
import db from "../../database/connection.js"

const router = express.Router()

const getAll = (database) => {
    return new Promise((resolve, reject) => {
        db.query(`SELECT * FROM ${database}`, 
          (error, results) => {
            if (error){
              return reject(error)
            } else {
              return resolve(results)
            }
          }
        )
      }
    )
}

const getUser = (field, value) => {
    return new Promise((resolve, reject) => {
        db.query(`SELECT * FROM user where ${field} IN("${value}")`, 
          (error, results) => {
            if (error){
              return reject(error)
            } else {
              return resolve(results)
            }
          }
        )
      }
    )
  }
  

const verify = async (password, hash) => {
    try {
      const match = await bcrypt.compare(password, hash)
      return match
    } catch (err) {
      throw err
    }
  }

const findUser = async (username, password) => {
  let content = await getAll("user")
  const userLocation = content.findIndex(user => user.email === username)

  try {
    if(userLocation != -1) {
      if(await verify(password, content[userLocation].password).then(valid => valid)){
        return true
      }
    }

    return false
    } catch (err) {
      console.log(err)
    }
}

const isValidObject = (obj, requiredProperties) => {
  let isValid = requiredProperties.every(property => obj.hasOwnProperty(property));
  return isValid;
}
  
const validateLogin = async (req, res, next) => {
    const requiredProperties = ["email", "password"];
    const validObject = isValidObject(req.body, requiredProperties);

    if(validObject){
      try {
        if(await findUser(req.body.email, req.body.password)){
          next();
        }
        else {
          return res.status(401).json({message: "incorrect credentials provided"});
        }
        } catch (err) {
        return next(err)
      }
    } 
    if(!validObject){
      return res.status(401).json({message: "incorrect credentials provided"});
  }
}

router.post('/', validateLogin, async (req, res) => {
  try {
    const email = req.body.email;
    const token = jwt.sign({email}, process.env.JWT_SECRET, {expiresIn: '60m'})
    const user = await getUser("email", email)
    return res.status(200).json({token, userID:user[0].userID, name:user[0].name, email:user[0].email })
  } catch (err) {
      console.log(err)
      return next(err)
  }
})

export default router