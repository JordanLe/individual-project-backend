# CSFS1040 - Course Project

# Author: Jordan Lee

## Set-up

Domain: portfolio.jordan-lee.me

Login Credentials are: Email: Jordan@Lee.com & Password: zxcvasdfqwer

Once logged in the top right should provide a dropdown menu to view Submissions, Add User, and Edit Portfolio.

You can enter entries for the Contact Page and submit them. You can view these under Submissions.

You can add users by logging in and going to Add User to enter a username and password. Once entered you can logout and login again with the new user.

You can edit the description of the projects within the Portfolio Page by logging in and going to Edit Portfolio. There you can edit within the text box and submit the changes.
