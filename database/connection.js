// db Connection w/ env
import mysql from "mysql"
const db = mysql.createConnection({
  socketPath: process.env.DATABASE_SOCKET,
  host : process.env.DATABASE_HOST,
  user : process.env.DATABASE_USER,
  password : process.env.DATABASE_PASSWORD,
  database : process.env.DATABASE_NAME
});

db.connect(function (err){
 if (err){
     console.log(err);
 } else{
     console.log("MySQL database is connected");
 }
});

export default db;
