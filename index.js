import express from "express";
import errorHandler from "./src/middleware/errorHandler.js";
import dotenv from "dotenv"
import cors from 'cors'
import usersRoutes from "./src/routes/users.js"
import loginRoutes from "./src/routes/login.js"
import contact_formRoutes from "./src/routes/contact_form.js"
import portfolioRoutes from "./src/routes/portfolio.js"

dotenv.config()

const router = express.Router();

//Test if postman is working
router.get("/", (req, res) => {
    return res.status(200).send("Hello World");
  });

//errorhandler
router.all("*", (req, res, next) => {
  let err = new Error("typed wrong URL");
  next(err);
});

const app = express();
const port = process.env.PORT || 3001

// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: false }))

// allows us to parse json 
app.use(express.json());
app.use(cors())

// parse application/json
app.use('/users', usersRoutes);
app.use('/login', loginRoutes);
app.use('/contact_form', contact_formRoutes);
app.use('/portfolio', portfolioRoutes);



app.use(errorHandler);

// App Server Connection
app.listen(process.env.PORT || 3001, () => {
  console.log(`app is running on port ${process.env.PORT || 3001}`)
})